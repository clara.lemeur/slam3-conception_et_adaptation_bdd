<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once 'vendor/autoload.php';

$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src"), true);

$connection = array(
    'driver' => 'pdo_pgsql',
    'host' => 'postgresql.bts-malraux72.net',
    'port' => '5432',
    'dbname' => 'c.lemeur',
    'user' => 'c.lemeur',
    'password' => 'P@ssword',
);

$entityManager = EntityManager::create($connection, $config);
?>