<?php

/**
* @Entity @Table(name="orm.instrument")
**/
class Instrument
{
    /**
    * @Id @Column(type="string", length=5)
    **/
    private $ref;
    /**
    * @Column(length=30) 
    **/
    private $nom;
    /**
    * @Column(length=20) 
    **/
    private $marque;
    /**
    * @Column(length=150) 
    **/
    private $caract;
    /**
    * @Column(type="integer") 
    **/
    private $prix;
    /**
    * @Column(length=20) 
    **/
    private $photo;
    /**
     * @ManyToOne(targetEntity="Categorie")
     * @JoinColumn (name="codeCateg", referencedColumnName="codeCategorie")
     * @var type 
     */
    private $codeCateg;


    // *** puis, constructeurs ainsi que méthodes get et set éventuelles
    // constructeur par défaut
    public function __construct()
    {
        $this->ref = NULL;
        $this->nom = NULL;
        $this->marque = NULL;
        $this->caract = NULL;
        $this->prix = 0;
        $this->photo = NULL;
    }
    
    // constructeur avec paramètres pour initialiser les propriétés de Categorie
    public function init($ref, $nom, $marque, $caract, $prix, $photo)
    {
        $this->ref = $ref;
        $this->nom = $nom;
        $this->marque = $marque;
        $this->caract = $caract;
        $this->prix = $prix;
        $this->photo = $photo;
    }
    
    public function getRef()
    {
        return $this->ref;
    }
    
    public function getNom()
    {
        return $this->nom;
    }
    
    public function getMarque()
    {
        return $this->marque;
    }
    
    public function getcaract()
    {
        return $this->caract;
    }
    
    public function getPrix()
    {
        return $this->prix;
    }
    
    public function getPhoto()
    {
        return $this->photo;
    }
    
    public function getCodeCateg()
    {
        return $this->codeCateg;
    }
}