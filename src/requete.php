<?php
require_once "../bootstrap.php";
/*
include "Categorie.php";
include "Instrument.php";
*/


// *** Récupération de toutes les categories et parcours du résultat pour affichage
$cats = $entityManager->getRepository('Categorie')->findAll();
foreach ($cats as $cat)
{
    echo $cat-> getNomCat()."<br>";
}


// *** Recherche d'un instrument avec affichage du nom de sa catégorie
$instru = new Instrument();
$instru = $entityManager->find('Instrument', 'BATPE');
echo "<br> <br>";
echo $instru->getNom();
echo "<br> <br>";
echo $instru->getCodeCateg()->getNomCat();


// *** Création d'un instrument
$instrument = new Instrument();
$instrument->init('MAGE', 'nom', 'marque', 'caract', 50, 'photo' );
$entityManager->persist($instrument);
$entityManager->flush();



// *** Modification d'une categorie
$cat->setNomCat('Modèle d\'étude - confirmé');
$entityManager->flush();



// *** Suppression d'un instrument
$unInstru = new Instrument();
$unInstru = $entityManager->find('Instrument', 'MAGE');
$entityManager->remove($unInstru);
$entityManager->flush();

?>