<!doctype html>
<html><html lang="en">
<head>
    <title>Catalogue</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" href="../images/book.png" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" type="text/css" href="../CSS/style.css" />
    <!--<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>     -->

    <!-- Script de la prof-->
    <script>
            function affiche(nom, nomPhoto, marque, caract, prix, categ)
            {
                    document.getElementById("titre").innerHTML=nom;
                    document.getElementById("imageInstrument").src=nomPhoto;
                    document.getElementById("marque").innerHTML=marque;
                    document.getElementById("caract").innerHTML=caract;
                    document.getElementById("prix").innerHTML=prix;
                    document.getElementById("categorie").innerHTML=categ;
            }
    </script>

</head>

<body class="bleu">


    <main>

        <div class="block">
             <div class="container">
                 <div class="titre">
                    <h1>Catalogue Percussion</h1>
                 </div>
                 <div class="row">
                    <div class="col-12 col-lg-4 col-sm-12 d-flex flex-row">
                       <div id="p-2 photos" >
                            <?php
                            require_once "../bootstrap.php";

                            $instruments = $entityManager->getRepository('Instrument')->findAll();
                            foreach ($instruments as $instrument)
                            {
                                $nom = $instrument-> getNom();
                                $marque = $instrument-> getMarque();
                                $caract = htmlspecialchars($instrument-> getCaract());
                                $prix = $instrument-> getPrix();
                                $photo = $instrument-> getPhoto();
                                $NameCat = $instrument-> getCodeCateg()-> getNomCat();
                                ?>
                            <div class="rounded cellule">
                                <img class="vignette" src="../images/<?php echo $photo; ?>" onclick="affiche('<?php echo $nom;?>', '../images/<?php echo $photo;?>', '<?php echo $marque;?>', '<?php echo $caract;?>', '<?php echo $prix;?>', '<?php echo $NameCat;?>')" /><br>
                                <?php echo $nom;?>
                            </div>
                                <?php
                            }
                            ?>
                       </div>
                    </div>

                    <div class="col-12 col-lg-8 col-sm-12 d-flex">
                        <div class="instrument" id="instrument">
                            <h2 id="titre"> <?php echo $instruments[0]->getNom(); ?></h2>

                            <img id="imageInstrument" src="../images/<?php echo $instruments[0]->getPhoto(); ?> "/>

                            <hr/>

                            <table id="fichedetail" class="fichedetail">
                                    <tr>
                                        <th colspan="2" id="titre_bloc">Fiche détaillée</th>
                                    </tr>
                                    <tr>
                                        <th>Marque</th>
                                        <td>
                                            <span id="marque">
                                                <?php echo $instruments[0]->getMarque(); ?>
                                            </span>
                                        </td>
                                    </tr>
                                     <tr>
                                        <th>Caracteristique</th>
                                        <td>
                                            <span id="caract">
                                                <?php echo $instruments[0]->getCaract(); ?>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Prix</th>
                                        <td>
                                            <span id="prix">
                                                <?php echo $instruments[0]->getPrix(); ?> €
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Catégorie</th>
                                        <td>
                                            <span id="categorie">
                                                <?php echo $instruments[0]->getCodeCateg()-> getNomCat(); ?>
                                            </span>
                                        </td>
                                    </tr>
                            </table>
                        </div>
                    </div>
                 </div>
            </div>

        </div>
    </main>


</body>
</html>