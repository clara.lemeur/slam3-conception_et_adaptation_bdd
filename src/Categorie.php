<?php

/**
* @Entity @Table(name="orm.categorie")
**/
class Categorie
{
    /**
    * @Id @Column(type="string", length=2)
    **/
    private $codeCategorie;
    /**
    * @Column(length=30) 
    **/
    private $nomCategorie;
    
    // *** puis, constructeurs ainsi que méthodes get et set éventuelles
    // constructeur par défaut
    public function __construct()
    {
    $this->codeCategorie = NULL;
    $this->nomCategorie = NULL;
    }
    
    // constructeur avec paramètres pour initialiser les propriétés de Region
    public function init($codeCategorie, $nomCategorie)
    {
    $this->codeCategorie = $codeCategorie;
    $this->nomCategorie = $nomCategorie;
    }
    public function getCodeCat()
    {
        return $this->codeCategorie;
    }
    
    public function getNomCat()
    {
        return $this->nomCategorie;
    }

    public function setNomCat($nomcat)
    {
        $this->nomCategorie = $nomcat;
    }

}